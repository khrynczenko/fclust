import numpy as np

from sklearn.datasets import make_blobs
from fclust.clustering import KMeans, FCM, FCMED
from fclust import utilities

np.random.seed(1)  # for reproducible results


def main():
    # fcm = FCM(iterations=5, fuzzifier=2.0)
    fcm = FCMED(iterations=5, fuzzifier=2.0)
    # fcm = KMeans(iterations=5)
    samples, classes = make_blobs(100, 2, centers=4, random_state=1)
    outputs = fcm.cluster_iterative(samples, 3)
    for output in outputs:
        utilities.plot_clustering_result(samples, output)


if __name__ == "__main__":
    main()
