import numpy as np

from sklearn.datasets import make_blobs
from fclust.clustering import KMeans, FCM, FCMED
from fclust import utilities

np.random.seed(1)  # for reproducible results


def main():
    fcm = FCM(iterations=10, fuzzifier=2.0)
    # fcm = FCMED(iterations=301, fuzzifier=2.0)
    # fcm = KMeans(iterations=3)
    samples, classes = make_blobs(100, 2, centers=4, random_state=1)
    output = fcm.cluster(samples, 3)
    print(output.predictions)
    print(output.loss)
    utilities.plot_clustering_result(samples, output)


if __name__ == "__main__":
    main()
