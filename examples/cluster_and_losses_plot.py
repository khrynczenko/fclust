import numpy as np

from matplotlib import pyplot as plt
from sklearn.datasets import make_blobs
from fclust.clustering import KMeans, FCM, FCMED
from fclust import utilities

np.random.seed(1)  # for reproducible results


def main():
    fcm = FCMED(iterations=50, fuzzifier=2.0)
    samples, classes = make_blobs(100, 2, centers=4, random_state=1)
    outputs = fcm.cluster_iterative(samples, 3)
    predictions = outputs[-1].predictions
    centroids = outputs[-1].centroids
    losses = [output.loss for output in outputs]

    fig = plt.figure()
    plt.subplot(1, 2, 1)
    plt.scatter(samples[:, 0], samples[:, 1],
                c=predictions.argmax(axis=1))
    plt.scatter(centroids[:, 0], centroids[:, 1], color="red")
    plt.xlabel("x1")
    plt.ylabel("x2")

    plt.subplot(1, 2, 2)
    plt.title("Losses")
    plt.plot(range(len(losses)), losses)
    plt.xlabel("iteration")
    plt.ylabel("loss")

    plt.show()


if __name__ == "__main__":
    main()
