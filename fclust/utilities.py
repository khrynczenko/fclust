import numpy as np

from matplotlib import pyplot as plt
from fclust.clustering import ClusteringAlgorithmOutput


def plot_clustering_result(samples: np.ndarray,
                           result: ClusteringAlgorithmOutput):
    """
    Plots categorized samples with the centroids.

    """
    predictions = result.predictions
    centroids = result.centroids
    classes = predictions if predictions.ndim == 1 else predictions.argmax(
        axis=1)

    plt.scatter(samples[:, 0], samples[:, 1], c=classes)
    plt.scatter(centroids[:, 0], centroids[:, 1], color="red")
    plt.xlabel("x1")
    plt.ylabel("x2")
    plt.show()
