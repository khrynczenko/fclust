"""
Clustering algorithms.

"""
import abc
import numpy as np
import itertools

from abc import ABC
from dataclasses import dataclass
from typing import List
from fclust.weighted_median import weighted_median


def choose_centroids_randomly(x: np.ndarray, n_centroids: int) -> np.ndarray:
    if n_centroids < 2:
        raise ValueError(f"Centroids number should be positive "
                         f"and bigger than 1 not {n_centroids}")
    if n_centroids > x.shape[0]:
        raise ValueError(f"Number of centroids must not be higher than "
                         f"samples. Now it is: "
                         f"centroids={n_centroids}; samples={x.shape[0]}")
    indices_to_choose_from = x.shape[0]
    chosen_indices = np.random.choice(indices_to_choose_from, n_centroids,
                                      replace=False)
    return x[chosen_indices]


@dataclass
class ClusteringAlgorithmOutput:
    centroids: np.ndarray
    predictions: np.ndarray
    loss: float


class ClusteringAlgorithm(ABC):
    """
    Each class that derives from Clustering Algorithm must implement
    `clsuter_iterative` method. It should return list where each element is
    clustering result for corresponding iteration. By default you also get
    `cluster` method which does the same but fives only final output.

    """

    @abc.abstractmethod
    def cluster_iterative(self, samples: np.array,
                          n_centroids: int) -> List[ClusteringAlgorithmOutput]:
        """
        Performs clustering algorithm on given set of samples and store result
        for every iteration.

        :param samples: Array of samples where row is a sample and columns are
        features.
        :param n_centroids: How many centroids should be used.
        :return: Centroids, predictions and objective function value for each
        consecutive iteration.
        """

    def cluster(self, samples: np.array,
                n_centroids: int) -> ClusteringAlgorithmOutput:
        return self.cluster_iterative(samples, n_centroids)[-1]


class KMeans(ClusteringAlgorithm):
    def __init__(self, iterations: int):
        self._iterations = iterations

    def cluster_iterative(self, samples: np.array,
                          n_centroids: int) -> List[ClusteringAlgorithmOutput]:
        n_samples = samples.shape[0]
        centroids = choose_centroids_randomly(samples, n_centroids)
        predictions = np.zeros(n_samples,
                               dtype=np.float32)
        results = []
        for _ in range(self._iterations):
            for i, sample in enumerate(samples):
                distances = [np.linalg.norm(sample - centroid) for centroid in
                             centroids]
                shortest_distance_index = np.argmin(distances)
                predictions[i] = shortest_distance_index

            for i in range(n_centroids):
                centroids[i] = np.mean(samples[predictions == i], axis=0)

            loss = 0.0
            for i in range(n_centroids):
                loss += np.linalg.norm(
                    samples[predictions == i] - centroids[i]) ** 2.0

            results.append(
                ClusteringAlgorithmOutput(np.array(centroids),
                                          np.array(predictions),
                                          loss))
        return results


class FCM(ClusteringAlgorithm):
    """
    Fuzzy C-means algorithm according to:
    https://en.wikipedia.org/wiki/Fuzzy_clustering
    Written based on description in paper
    "A Comparative Study of Hard and Fuzzy Data Clustering Algorithmswith
    Cluster Validity Indices" by Mohamed Jafar et al.

    """

    def __init__(self, iterations: int, fuzzifier: float = 2.0):
        if fuzzifier < 1.0:
            raise ValueError(f"Fuzzifier must bust be in "
                             f"range [1, inf) not {fuzzifier}.")
        self._fuzzifier = fuzzifier
        self._iterations = iterations

    def cluster_iterative(self, samples: np.array,
                          n_centroids: int) -> List[ClusteringAlgorithmOutput]:
        n_samples = samples.shape[0]
        centroids = choose_centroids_randomly(samples, n_centroids).astype(
            np.float64)
        centroids = centroids + 0.1
        distances = np.zeros((n_samples, n_centroids), dtype=np.float64)
        membership = np.random.rand(n_samples, n_centroids)
        membership = membership / membership.sum(axis=1, keepdims=True)
        m = self._fuzzifier
        results = []

        for _ in range(self._iterations):

            # Calculate centroids
            for j in range(n_centroids):
                nominator = 0.0
                denominator = 0.0
                for i in range(n_samples):
                    nominator += (membership[i, j] ** m) * samples[i, :]
                    denominator += membership[i, j] ** m
                centroids[j, :] = nominator / denominator

            # Calculate distances
            for i in range(n_samples):
                distances[i] = [np.linalg.norm(samples[i] - centroid) for
                                centroid in
                                centroids]

            # Update membership
            for i in range(n_samples):
                for j in range(n_centroids):
                    denominator = 0.0
                    for k in range(n_centroids):
                        denominator += (distances[i, j] / distances[i, k]) ** (
                                2 / (m - 1))
                    membership[i, j] = 1 / denominator

            # Calculate objective function value
            loss = 0.0
            for i in range(n_samples):
                for j in range(n_centroids):
                    loss += membership[i, j] ** m * np.linalg.norm(
                        samples[i, :] - centroids[j, :]) ** 2
            results.append(
                ClusteringAlgorithmOutput(np.array(centroids),
                                          np.array(membership),
                                          loss))
        return results


class FCMED(ClusteringAlgorithm):
    """
    Fuzzy C-medians algorithm according to
    "Fuzzy Order Statistics and Their Application to Fuzzy Clustering" by
    Paul R. Kersten.

    """

    def __init__(self, iterations: int, fuzzifier: float = 2.0):
        if fuzzifier < 1.0:
            raise ValueError(f"Fuzzifier must bust be in "
                             f"range [1, inf) not {fuzzifier}.")
        self._fuzzifier = fuzzifier
        self._iterations = iterations

    def cluster_iterative(self, samples: np.array,
                          n_centroids: int) -> List[ClusteringAlgorithmOutput]:
        n_samples = samples.shape[0]
        centroids = choose_centroids_randomly(samples, n_centroids).astype(
            np.float64)
        centroids = centroids + 0.1
        distances = np.zeros((n_samples, n_centroids), dtype=np.float64)
        membership = np.random.rand(n_samples, n_centroids)
        membership = membership / membership.sum(axis=1, keepdims=True)
        m = self._fuzzifier
        results = []

        for _ in range(self._iterations):

            memberships_with_indices = np.concatenate(
                (np.zeros((n_samples, 1)), membership[:, :]),
                axis=1)  # first "index" column will be needed later for sorting
            memberships_with_indices[:, 0] = [i for i in range(
                n_samples)]  # make indices
            indices = np.argmax(membership,
                                axis=1)  # retrieves indices of columns with biggest values for each row

            # Calculate centroids
            for j in range(n_centroids):
                c = memberships_with_indices[indices == j,
                    :]  # get those rows which has highest membership for being in j centroid
                c = c[c[:,
                      j + 1].argsort()]  # j + 1 because additonal index column
                index_of_median = weighted_median(c[:, 0], c[:, j + 1])
                centroids[j, :] = samples[int(index_of_median)]

            # Calculate distances
            for i in range(n_samples):
                distances[i] = [np.linalg.norm(samples[i] - centroid, ord=1)
                                for
                                centroid in
                                centroids]
            distances += 0.0001  # So there will be no division by 0

            for i in range(n_samples):
                for j in range(n_centroids):
                    denominator = 0.0
                    for k in range(n_centroids):
                        denominator += (distances[i, j] / distances[i, k]) ** (
                                1 / (m - 1))
                    membership[i, j] = 1 / denominator

            # Calculate objective function value
            loss = 0.0
            for i in range(n_samples):
                for j in range(n_centroids):
                    loss += membership[i, j] ** m * np.linalg.norm(
                        samples[i, :] - centroids[j, :], ord=1)
            results.append(
                ClusteringAlgorithmOutput(np.array(centroids),
                                          np.array(membership),
                                          loss))
        return results
